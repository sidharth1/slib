/* Copyright 2015, 2016 Sidharth Jha.
 * This file is distributed under the terms of the
 * GNU Lesser General Public License (LGPL).
 */

// This file defines filesystem extensions.

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <algorithm>
#include <cstring>
#include <list>
#include <string>

namespace slib { namespace filesystem { inline namespace v1 {
    class Path
    {
    public:
        using const_char_t = const char;
        using const_int_t = const int;
        using const_string_t = const std::string;
        using string_t = std::string;
        using string_it = std::string::iterator;
        using string_rit = std::string::reverse_iterator;
        using list_t = std::list<std::string>;
        using str_list_it = std::list<std::string>::iterator;
        using str_list_rit = std::list<std::string>::reverse_iterator;

        Path (const Path&) = default;
        Path (const Path&&) = default;

        Path& operator= (const Path&) = default;

        Path () = default;

        // TODO:
        // - detect absolute path.
        // - determine unix/win paths, etc.
        Path (const_string& p_str);

        // Detect if the path is unix like.
        bool is_nix ();
        // Same as above but detect if the path is windows like.
        bool is_win ();

        // Convert path to nix.
        Path& to_nix ();
        // Convert path to windows.
        Path& to_win ();

    private:
        string_t abs_path; // Contains the absolute path.
        a_const_char nix_ch = '/';
        a_const_char win_ch = '\\';
    }; // End class Path

}}}; // End namespaces slib, filesystem, v1

#endif // FILESYSTEM_H

