/* Copyright 2015, 2016 Sidharth Jha.
 * This file is distributed under the terms of the
 * GNU Lesser General Public License (LGPL).
 */

#include "string-extensions.hpp"

namespace slib { namespace ext { inline namespace v1 {

    template <typename Container_t>
    inline void remove (Container_t& c, const_char_t& val)
    {
        c.erase(remove(c.begin(), c.end(), val), c.end());
    }

    bool str_begins_with (const_string_t& str1, const_string_t& str2)
    {
        if (str1.size() < str2.size()) {
            return false;
        } else {
            return strncmp(str2.c_str(), str1.c_str(), str2.size()) == 0;
        }
    }

    bool str_begins_withi (const_string_t& str1, const_string_t& str2)
    {
        if (str1.size() < str2.size()) {
            return false;
        } else {
            return str_strncmpi(str2.c_str(), str1.c_str(), str2.size()) == 0;
        }
    }

    size_t str_chomp (string_t& str)
    {
        size_t count = 0;

        while (!str.empty() && str.at(str.size() - 1) == '\n') {
            str = str.substr(0, str.size() - 1);
            count++;
        }

        return count;
    }

    size_t str_chomp (str_list& container)
    {
        size_t count = 0;

        for (string_t& str : container) {
            count += str_chomp(str);
        }

        return count;
    }

    char str_chop (string_t& str)
    {
        char result = 0;

        if (!str.empty()) {
            result = str.at(str.size() - 1);
            str = str.substr(0, str.size() - 1);
        }

        return result;
    }

    char str_chop (str_list& container)
    {
        char result = 0;

        for (string_t& str : container){
            result = str_chop(str);
        }

        return result;
    }

    bool str_ends_with (const_string_t& str1, const_string_t& str2)
    {
        if (str1.size() < str2.size()) {
            return false;
        } else {
            string_t s1 = str1.substr(str1.size() - str2.size());
            return s1 == str2;
        }
    }

    bool str_ends_withi (const_string_t& str1, const_string_t& str2)
    {
        if (str1.size() < str2.size()) {
            return false;
        } else {
            string_t s1 = str1.substr(str1.size() - str2.size());
            return str_strncmpi(str2.c_str(), s1.c_str(), str2.size()) == 0;
        }
    }

    bool str_has_substring (const_string_t& str1, const_string_t& str2)
    {
        if (str2.empty() || str1.size() < str2.size()) {
            return false;
        }

        return str1.find(str2) != string_t::npos;
    }


    bool str_has_substringi (const_string_t& str1, const_string_t& str2)
    {
        if (str2.empty() || str1.size() < str2.size()) {
            return false;
        }

        return strcasestr(str1.c_str(), str2.c_str()) != 0;
    }

    void str_join (string_t& str,
                   const str_list& container,
                   const_char_t& delimiter)
    {
        if (container.size() == 0 || delimiter == 0) {
            return;
        }

        for (string_t s : container) {
            str += (s + delimiter);
        }

        if (!str.empty()) {
            str.erase(str.size() - 1);
        }
    }

    void str_replace (const_string_t& pattern,
                      string_t& str,
                      const_string_t& new_pattern)
    {
        if (pattern.empty() ||
            str.empty()     ||
            pattern == new_pattern) {
            return;
        }

        size_t pos = str.find(pattern);
        while (pos != string_t::npos) {
            str.replace(pos, pattern.size(), new_pattern);
            pos = str.find(pattern);
        }
    }

    void str_replace_first (const_string_t& pattern,
                            string_t& str,
                            const_string_t& new_pattern)
    {
        if (pattern.empty() ||
            str.empty()     ||
            pattern == new_pattern) {
            return;
        }

        size_t pos = str.find(pattern);
        if (pos != string_t::npos) {
            str.replace(pos, pattern.size(), new_pattern);
        }
    }

    void str_replace_last (const_string_t& pattern,
                           string_t& str,
                           const_string_t& new_pattern)
    {
        if (pattern.empty() ||
            str.empty()     ||
            pattern == new_pattern) {
            return;
        }

        size_t pos = str.rfind(pattern);
        if (pos != string_t::npos) {
            str.replace(pos, pattern.size(), new_pattern);
        }
    }

    size_t str_strcmpi (const_char_t* s1, const_char_t* s2)
    {
        if (s1 == s2) {
            return 0;
        } else if (!s2) {
            return 1;
        } else if (!s1) {
            return -1;
        }

        return str_strncmpi(s1,
                            s2,
                            strlen(s1) > strlen(s2) ?
                            strlen(s1) : strlen(s2));
    }

    size_t str_strncmpi (const_char_t* s1, const_char_t* s2, size_t n)
    {
        size_t i = 0;
        unsigned char a, b;
        while (i < n) {
            a = tolower((unsigned char) s1[i]);
            b = tolower((unsigned char) s2[i]);
            if (a != b) {
                return a - b;
            } else if (a == b && a == '\0') {
                // End of both strings are reached,
                // so we are done.
                return 0;
            }
            ++i;
        }
        // 'n' characters were successfully compared.
        return 0;
    }

    void str_split (str_list& container,
                    const_string_t& str,
                    const_string_t& delimiters)
    {
        if (str.empty() || delimiters.empty()) {
            return;
        }

        size_t len = str.size();
        string_t word;
        char ch;
        for (size_t i = 0; i < len; ++i) {
            ch = str.at(i);
            if (delimiters.find(ch) == string_t::npos) {
                word += ch;
            } else {
                container.push_back(word);
                word.clear();
            }
        }

        if (!word.empty()) {
            container.push_back(word);
        }
    }

    void str_split (str_list& container,
                    const_string_t& str,
                    const_char_t& delimiter)
    {
        if (str.empty() || delimiter == 0) {
            return;
        }

        size_t len = str.size();
        string_t word;
        char ch;
        for (size_t i = 0; i < len; ++i) {
            ch = str.at(i);
            if (ch != delimiter) {
                word += ch;
            } else {
                container.push_back(word);
                word.clear();
            }
        }

        if (!word.empty()) {
            container.push_back(word);
        }
    }

    void str_split_keep_quotes (str_list& container,
                                const_string_t& str,
                                const_char_t& delimiter)
    {
        if (str.empty()    ||
            delimiter == 0 ||
            delimiter == '"') {
            return;
        }

        bool inquotes = false;
        char ch = 0; // current char
        string_t word = ""; // word to push

        size_t i = 0;
        size_t len = str.size();
        while (i < len) {
            ch = str[i];

            if (ch == '"') {
                inquotes = !inquotes;
            } else if (!inquotes && ch == delimiter) {
                container.push_back(word);
                word.clear();
                ++i;
                continue;
            }

            word += ch;
            ++i;
        } // end while loop

        if (!word.empty()) {
            container.push_back(word);
        }
    }

    void str_strip_quotes (string_t& str, size_t pos)
    {
        if (pos < 0 || str.empty()) {
            return;
        }

        string_t s = str.substr(pos);
        remove(s, '"');
        str = str.substr(0, pos) + s;
    }

    void str_strip_quotes_outer (string_t& str)
    {
        if (str.empty()) {
            return;
        }

        size_t begin_quote = str.find('"');
        size_t end_quote = str.rfind('"');

        if (begin_quote == end_quote    ||
            begin_quote == string_t::npos ||
            end_quote == string_t::npos) {
            return;
        }

        size_t len = str.size();
        while (begin_quote < len &&
               end_quote >= 0) {
            if (str.at(begin_quote) == '"' &&
                str.at(end_quote) == '"') {
                // Erase if we have a matching end quote
                // for the begin quote.
                str.erase(begin_quote, 1);
                str.erase(--end_quote, 1);
                --end_quote;
            } else {
                break;
            }
        } // End while loop.
    }

    string_t str_substr_lower (const_string_t& str,
                               size_t pos,
                               size_t len)
    {
        if (pos > len || pos < 0 || len < 0) {
            return "";
        }

        string_t result = str.substr(pos, len);
        str_tolower(result);

        return result;
    }

    string_t str_substr_upper (const_string_t& str,
                               size_t pos,
                               size_t len)
    {
        if (pos > len || pos < 0 || len < 0) {
            return "";
        }

        string_t result = str.substr(pos, len);
        str_toupper(result);

        return result;
    }

    void str_tolower (string_t& str)
    {
        size_t i = 0;
        for (char& ch : str) {
            str[i] = tolower(ch);
            ++i;
        }
    }

    void str_toupper (string_t& str)
    {
        size_t i = 0;
        for (char& ch : str) {
            str[i] = toupper(ch);
            ++i;
        }
    }

    // Removes occurences of the character 'val' from the
    // string 'str'. Range may be -1, 0, or 1, for leading,
    // all, or trailing respectively.
    // REQUIRED: non-empty string/val, and valid range.
    size_t str_trim (string_t& str, const_char_t& val, const_int_t& range)
    {
        const size_t str_len = str.size();

        if (range == 0) {
            remove(str, val);
        } else if (range == 1) {
            string_rit rit = str.rbegin();
            while (rit != str.rend() && *rit == val) {
                ++rit;
                str.erase(rit.base());
            }
        } else if (range == -1) {
            string_it it = str.begin();
            while (it != str.end() && *it == val) {
                str.erase(it);
            }
        }

        return str_len - str.size();
    }

    size_t str_trimWS (string_t& str)
    {
        if (str.empty()) {
            return 0;
        }

        return str_trimWS_leading(str) + str_trimWS_trailing(str);
    }

    size_t str_trimWS (str_list& container)
    {
        if (container.empty()) {
            return 0;
        }

        size_t result = 0; // total number of whitespaces.
        for (string_t& str : container) {
            result += str_trimWS(str);
        }

        return result;
    }

    size_t str_trimWS_leading (string_t& str)
    {
        if (str.empty()) {
            return 0;
        }

        return str_trim(str, ' ', -1); // -1 - remove leading.
    }

    size_t str_trimWS_leading (str_list& container)
    {
        if (container.empty()) {
            return 0;
        }

        size_t result = 0; // total number of whitespaces.
        for (string_t& str : container) {
            result += str_trimWS_leading(str);
        }

        return result;
    }

    size_t str_trimWS_trailing (string_t& container)
    {
        if (container.empty()) {
            return 0;
        }

        return str_trim(container, ' ', 1); // 1 - remove trailing.
    }

    size_t str_trimWS_trailing (str_list& container)
    {
        if (container.empty()) {
            return 0;
        }

        size_t result = 0; // total number of whitespaces.
        for (string_t& str : container) {
            result += str_trimWS_trailing(str);
        }

        return result;
    }

}}}; // End namespaces slib, ext, v1

