/* Copyright 2015, 2016 Sidharth Jha.
 * This file is distributed under the terms of the
 * GNU Lesser General Public License (LGPL).
 */

// This file defines simple string extensions.

#ifndef STRING_EXTENSIONS_H
#define STRING_EXTENSIONS_H

#include <algorithm>
#include <cstring>
#include <list>
#include <string>

namespace slib { namespace ext { inline namespace v1 {

    using const_char_t = const char;
    using const_int_t = const int;
    using const_string_t = const std::string;
    using string_t = std::string;
    using string_it = std::string::iterator;
    using string_rit = std::string::reverse_iterator;
    using str_list = std::list<std::string>;
    using str_list_it = std::list<std::string>::iterator;
    using str_list_rit = std::list<std::string>::reverse_iterator;

    // Check if str1 begins with str2.
    bool str_begins_with  (const_string_t& str1, const_string_t& str2);
    // Same as above but case-insensitive.
    bool str_begins_withi (const_string_t& str1, const_string_t& str2);

    // Matches and removes any trailing newlines '\n' in the string.
    // Returns the total number of new lines removed, and 0 otherwise.
    size_t str_chomp (string_t& str);
    // Does the same as above  for each string in a list.
    // Returns the total number of newlines removed (for all strings)
    // and 0 otherwise.
    size_t str_chomp (str_list& container);
    // Strips  and returns the last character in a string.
    // Otherwise returns 0.
    char str_chop (string_t& str);
    // Same as above except it returns the last character
    // of the last string in a list. 0 otherwise.
    char str_chop (str_list& container);

    // Check if str1 ends with str2.
    bool str_ends_with  (const_string_t& str1, const_string_t& str2);
    // Same as above but case-insensitive.
    bool str_ends_withi (const_string_t& str1, const_string_t& str2);

    // Check if str1 (haystack) contains str2 (needle).
    // Returns: true if found and false otherwise.
    bool str_has_substring (const_string_t& str1, const_string_t& str2);
    // Same as above but case-insensitive.
    bool str_has_substringi (const_string_t& str1, const_string_t& str2);

    // Join a list of strings into a string based on
    // a given delimiter (default: ' ').
    // Example:
    // list = {a, b, c}
    // delimiter = ' '
    // string = "a b c"
    void str_join (string_t& str,
                   const str_list& container,
                   const_char_t& delimiter = ' ');

    // Replaces all occurences of 'pattern' in 'str'
    // with 'new_pattern'.
    void str_replace (const_string_t& pattern,
                      string_t& str,
                      const_string_t& new_pattern);
    // Same as above but only replace the first occurence.
    void str_replace_first (const_string_t& pattern,
                            string_t& str,
                            const_string_t& new_pattern);
    // Same as above but only replace the last occurence.
    void str_replace_last (const_string_t& pattern,
                           string_t& str,
                           const_string_t& new_pattern);

    // Splits a string into a list based on
    // the given delimiter (default: ' ').
    // Example:
    // string = foo|bar
    // delimiter = '|'
    // list = {foo, bar}
    void str_split (str_list& container,
                    const_string_t& str,
                    const_char_t& delimiter = ' ');
    // Splits a string into a list based on
    // any found delimiters (default: ' ').
    // Example:
    // string = cat,dog,tiger;animals.
    // delimiters = ",;"
    // list = {cat, dog, tiger, animals.}
    void str_split (str_list& container,
                    const_string_t& str,
                    const_string_t& delimiters = " ");
    // Splits a string based on a delimiter (default: ' ') into a list.
    // Ignores the presence of the delimiter within quotes.
    // Assumption: No nested quotes (e.g., "foo"bar"baz""").
    // Example:
    // string = fo|o"ba|r"|baz
    // delimiter = '|'
    // list = {fo, o"ba|r", baz}
    void str_split_keep_quotes (str_list& container,
                                const_string_t& str,
                                const_char_t& delimiter = ' ');

    // Function to strip all quotes from a string.
    // Example:
    // string a = \"FOO\" \"BAR\"
    // result = FOO BAR
    void str_strip_quotes (string_t& str, size_t pos = 0);
    // Similar to above but only strips the first
    // found outermost quotes.
    // Example:
    // string a = A \"\"\"FOO \"BAZ\" BAR\"\" B
    // result = A \"FOO \"BAZ\" BAR B
    void str_strip_quotes_outer (string_t& str);

    // Function same as strcmp/strncmp but case agnostic.
    size_t str_strncmpi (const_char_t* s1, const_char_t* s2, size_t n);
    size_t str_strcmpi (const_char_t* s1, const_char_t* s2);

    // Return a substring in lower case.
    string_t str_substr_lower (const_string_t& str,
                               size_t pos = 0,
                               size_t len = string_t::npos);
    // Same as above but upper case.
    string_t str_substr_upper (const_string_t& str,
                               size_t pos = 0,
                               size_t len = string_t::npos);

    // Simple conversion of string to bool.
    // "true" => true, anything else is false.
    inline bool str_to_bool (const_string_t& str) {
        return str == "true";
    }

    // Convert string to lowercase.
    void str_tolower (string_t& str);
    // Convert string to uppercase.
    void str_toupper (string_t& str);

    // Removes any leading, and trailing whitespaces, and
    // returns the total number of whitespaces removed.
    size_t str_trimWS (string_t& str);
    // Overload does the same for each element in the list.
    size_t str_trimWS (str_list& str);
    // Same as above but only remove leading.
    size_t str_trimWS_leading (string_t& str);
    size_t str_trimWS_leading (str_list& str);
    // Same as above but only remove trailing.
    size_t str_trimWS_trailing (string_t& str);
    size_t str_trimWS_trailing (str_list& str);

}}}; // End namespaces slib, ext, v1

#endif // STRING_EXTENSIONS_H

