/* Copyright 2015, 2016 Sidharth Jha.
 * This file is distributed under the terms of the
 * GNU Lesser General Public License (LGPL).
 */

// Can print at various levels of verbosity.
// Use case:
// 1> The verbosity flag can be determined and set at the beginning
//    of the program via a command line switch.
// 2> Once the parameter is detected, the static variable 'verbosity_level'
//    can be set to ALWAYS, DEBUG, or ERROR.
//    NOTE: ALWAYS will print to stdout agnostic of 'verbosity_level'.
// 3> After this, any VPRINT statements containing that verbosity level will
//    show their output (NOTE: ERROR prints to stderr).
//
// Example:
//      $ run_some_prog --verbose=debug
//
//      ...
//      #include "verbosity.hpp"
//
//      using namespace slib::verbosity;
//
//      void foo () {
//          vector<int> vec = {1,2,3,4,5};
//          VPRINT(DEBUG, "VEC: " << v << "with "
//                                << v.size() << " elements." << endl);
//      }
//
//      void bar () {
//          string str = "foo bar.";
//          VPRINT(ERROR, str);
//      }
//
//      int main (int argc, char* argv[])
//      {
//          ...
//          if (argv[1] == "--verbose=debug")
//              verbosity_level = DEBUG;
//          ...
//          foo(); // This will print!
//          bar(); // This will NOT print!
//          ...
//      }

#ifndef VERBOSITY_HPP
#define VERBOSITY_HPP

#include <iostream>
#include <list>
#include <vector>

namespace slib { namespace verbosity { inline namespace v1 {

    using std::cout;
    using std::cerr;
    using std::endl;

#define VPRINT(x, y) { \
    if (x == ALWAYS) { \
        std::cout << y; \
    } else if (verbosity_level == x) { \
        switch(verbosity_level) { \
            case DEBUG: \
                std::cout << y; \
                break; \
            case ERROR: \
                std::cerr << y; \
                break; \
            default: \
                break; \
        } \
    } \
}


    typedef enum verbosity
    {
        ALWAYS = 1,
        DEBUG = 3,
        ERROR = 2
    } verbosity_t;

    static verbosity_t verbosity_level = ALWAYS;

    inline void set_verbosity (verbosity_t vt)
    {
        verbosity_level = vt;
    }

    template
    <
        typename T,
        template<typename, typename=std::allocator<T>>
        class Container
    >
    inline std::ostream& operator<< (std::ostream& target,
                                     const Container<T>& source)
    {
        auto it = source.begin();

        target << "{";
        bool first = true;
        while(it != source.end()) {
            if (!first) {
                target << ',';
            } else {
                first = false;
            }
            target << *it;
            ++it;
        }
        target << "}";

        return target;
    }

}}}; // End namespaces slib, verbosity, v1

#endif // VERBOSITY_HPP

